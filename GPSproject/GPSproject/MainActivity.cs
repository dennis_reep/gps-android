﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using Java.IO;
using System;
using Android.Locations;
using Android.Content;
using Android.Runtime;

namespace GPSproject
{
	[Activity (Label = "GPS Test", MainLauncher = true, Icon = "@mipmap/icon")]
	public class MainActivity : Activity, ILocationListener
	{
		public void OnLocationChanged (Location location)
		{
			System.Console.WriteLine (location.Latitude.ToString ().Replace (',', '.') + ", " + location.Longitude.ToString ().Replace (',', '.'));
			Toast.MakeText (this, location.Latitude.ToString ().Replace (',', '.') + ", " + location.Longitude.ToString ().Replace (',', '.'), ToastLength.Long).Show ();
		}

		public void OnProviderDisabled (string provider)
		{
			Toast.MakeText (this, "GPS Provider Disabled", ToastLength.Long).Show ();
		}

		public void OnProviderEnabled (string provider)
		{
			Toast.MakeText (this, "GPS Provider Enabled", ToastLength.Long).Show ();
		}

		public void OnStatusChanged (string provider, [GeneratedEnum] Availability status, Bundle extras)
		{
			Toast.MakeText (this, "GPS Status Changed to " + status.ToString(), ToastLength.Long).Show ();
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			LocationManager locationManager = (LocationManager)GetSystemService (LocationService);

			if (locationManager.IsProviderEnabled (LocationManager.NetworkProvider)) {
				locationManager.RequestLocationUpdates (LocationManager.NetworkProvider, 0, 0, this);
			} else {
				Toast.MakeText (this, "GPS Provider is not enabled", ToastLength.Long).Show ();
			}
		}

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.Main);
		}
	}
}

